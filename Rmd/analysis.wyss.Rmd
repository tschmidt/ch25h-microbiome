---
title: "analysis.wyss"
author: "Sebastian Schmidt"
date: "2/12/2019"
output: html_document
---

This script bundles analyses on the `Wyss` sub-experiment.

In particular, the following aspects are addressed in the respective sections below:

* Is the genotype associated to baseline differences in alpha diversity?
* Are there systematic shifts in community composition (beta diversity) between genotypes?
* Are the abundances of individual taxa (at different taxonomic resolutions) associated to the genotype?

Metadata and scripts required to generate the datasets used in the following analyses are available from <https://git.embl.de/tschmidt/ch25h-microbiome>.

********

## Environment Preparation

Load Libraries, set parameters, load sample metadata.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r Environment Preparation}
#Load packages
library("gplots", quietly=T)
library("ggplot2", quietly=T)
library("RColorBrewer", quietly=T)
library("VennDiagram", quietly=T)
library("Matrix", quietly=T)
library("reshape2", quietly=T)
library("plyr", quietly=T)
library("phyloseq", quietly=T)
library("vegan", quietly=T)
library("ape", quietly=T)
library("edgeR", quietly=T)
library("emmeans", quietly=T)

#Set basic parameters
PARAM <- list()
PARAM$folder.Rmd <- getwd()
PARAM$folder.base <- gsub("Rmd", "", PARAM$folder.Rmd)
PARAM$folder.data <- paste0(PARAM$folder.base, "/data/")
PARAM$folder.results <- paste0(PARAM$folder.base, "/results/")

#Load data
load(paste0(PARAM$folder.data, "data.otu.Rdata"))
load(paste0(PARAM$folder.data, "data.sample.Rdata"))
load(paste0(PARAM$folder.data, "data.beta_div.wyss.Rdata"))
```

Subset data and adjust factor levels if necessary.

```{r}
#Filter data to relevant samples (H2O treatment only and non-co-housed only)
#=> data from first run were endpoint measurements, after 79d or 8d
samples <- rownames(data.sample)[data.sample$experiment %in% c("AW12.02", "AW14.01", "AW14.02", "AW19.01", "AW19.02", "AW19.03", "AW19.04", "AW19.05")]
data.sample <- data.sample[samples, ]

data.sample$genotype <- factor(data.sample$genotype, levels = c("WT", "KO"))

#Filter OTU data according to current sample set.
ot <- ot[rowSums(ot[, samples]) > 0, samples]
otus <- rownames(ot)
data.otu <- data.otu[otus, ]
tree.otu <- drop.tip(tree.otu, tree.otu$tip.label[!tree.otu$tip.label %in% otus])
```

********
********

## Differences between Genotypes in Local Diversity (Alpha Diversity)

```{r Alpha Diversity, fig.height=6, fig.width=8}
#Reformat alpha diversity data.frame for plotting
alpha.long <- melt(data.sample, id.vars=c("experiment", "mouse_id", "genotype"), measure.vars=c("q.0", "q.1", "q.2"), variable.name="Diversity_Type")

#Generate plot for WT and KO
alpha.long.sub <- alpha.long[alpha.long$Diversity_Type == "q.0", ]
p.alpha.WT_KO <- ggplot(alpha.long.sub, aes(x=genotype, y=value, fill=genotype, color=genotype)) +
  #Generate boxplots
  geom_boxplot(alpha=0.5, outlier.color=NA, position = position_dodge(width=0.8)) +
  #Add jittered points
  geom_point(position=position_jitterdodge(jitter.width = 0.2, dodge.width = 0.8), size=1) +
  #Scale fill and colour
  scale_fill_manual(values = c("#1f78b4", "#e31a1c")) +
  scale_colour_manual(values = c("#1f78b4", "#e31a1c")) +
  ylab("OTU richness (rarefied)") +
  #MAnually set ylim
  ylim(0, 100) +
  #Set theme to pretty
  theme_minimal() +
  theme(
    axis.title.x = element_blank()
  ) +
  NULL
p.alpha.WT_KO

#Save plot
ggsave(p.alpha.WT_KO, filename=paste0(PARAM$folder.results, "wyss.alpha_div.pdf"), width=12, height=10, units = "cm", useDingbats=F)
```

```{r}
writeLines("Richness q.0")
aov.q0 <- aov(q.0 ~ genotype, data=data.sample)
print(summary(aov.q0))
```

********
********

## Differences between Genotypes in Community Composition (Beta Diversity)

Generate an ordination plot (PCoA).

```{r Beta Diversity, fig.height=6, fig.width=6}
curr.samples <- rownames(data.sample)
curr.beta <- as.matrix(beta.div[["Bray_Curtis"]])[curr.samples, curr.samples]

#Perform PCoA
curr.pcoa <- pcoa(curr.beta, rn=curr.samples)

#Prepare data for plotting
plot.data <- data.frame(data.sample[curr.samples, ], Axis.1=curr.pcoa$vectors[, "Axis.1"], Axis.2=curr.pcoa$vectors[, "Axis.2"], Group=data.sample$genotype);
#Get percent variation explained
if (curr.pcoa$correction[1] == "none") {
  plot.var_explained <- round(100*curr.pcoa$values[1:2, "Relative_eig"]/sum(curr.pcoa$values[, "Relative_eig"]), digits=1)
} else {
  plot.var_explained <- round(100*curr.pcoa$values[1:2, "Rel_corr_eig"]/sum(curr.pcoa$values[, "Rel_corr_eig"]), digits=1)
}
#Get convex hulls around points
plot.hulls <- ddply(plot.data, "Group", function(df) df[chull(df$Axis.1, df$Axis.2), ])
#Get centroids
plot.centroids <- ddply(plot.data, "Group", function(df) c(mean(df$Axis.1), mean(df$Axis.2)))
curr.plot.df <- merge(plot.data, plot.centroids, by="Group")

#Plot ordination
p.beta_div <- ggplot(curr.plot.df, aes_string(x="Axis.1", y="Axis.2", colour="genotype")) +
  #Add group centroids
  geom_point(data=curr.plot.df, aes(x=V1, y=V2, color=genotype), shape=15, size=5, alpha=0.6) +
  #Add segments of points to centroids
  geom_segment(data=curr.plot.df, aes(x=Axis.1, y=Axis.2, xend=V1, yend=V2), alpha=0.6) +
  #Add points (per sample)
  geom_point(size=3, alpha=0.6) +
  #Add ellipses (at 95%)
  #stat_ellipse(level=0.95, segments=101, alpha=0.8) +
  #Add convex hull around all points per group
  geom_polygon(data = plot.hulls, aes(fill=genotype), color=NA, alpha = 0.1) +
  #Add x & y axis labels
  xlab(paste("Axis 1 [", plot.var_explained[1], "%]", sep="")) +
  ylab(paste("Axis 2 [", plot.var_explained[2], "%]", sep="")) +
  #Scale fill and colour
  scale_fill_manual(values = c("#1f78b4", "#e31a1c")) +
  scale_colour_manual(values = c("#1f78b4", "#e31a1c")) +
  #Set overall plot theme
  theme_minimal() +
  #Amend plot theme
  theme(
    #legend.position = "none"
  ) +
  NULL
p.beta_div
#Save plot
ggsave(p.beta_div, filename=paste0(PARAM$folder.results, "wyss.beta_div.pdf"), width=15, height=15, units = "cm", useDingbats=F)
```

Test dissimilarities in a multivariate `PERMANOVA`.

```{r}
curr.aov <- adonis2(as.dist(curr.beta) ~ genotype, data=data.sample, permutations = 9999, parallel = 4)
curr.aov
```



