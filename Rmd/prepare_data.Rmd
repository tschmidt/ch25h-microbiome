---
title: "Prepare Data"
author: "Sebastian Schmidt"
date: "2019-02"
output: html_document
---

This script performs several preparatory steps on raw OTU and sample data:

* Load OTU table, OTU data (taxonomies),  and sample metadata
* Filter data by sample size and OTU occurrence
* Calculate per-sample alpha diverisities at different levels of rarefaction
* Perform some preliminary sanity checks on the data (OTU size distributions, etc.)

All data is available from <https://github.com/meringlab/2016_Spalinger_et_al>.

********
## Environment Preparation

Load Libraries, set parameters, load sample metadata.

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r Environment Preparation}
#Load packages
library("gplots", quietly=T)
library("ggplot2", quietly=T)
library("RColorBrewer", quietly=T)
library("VennDiagram", quietly=T)
library("Matrix", quietly=T)
library("reshape2", quietly=T)
library("plyr", quietly=T)
library("readxl", quietly=T)
library("phyloseq", quietly=T)
library("vegan", quietly=T)
library("ape", quietly=T)
library("phytools", quietly=T)
library("edgeR", quietly=T)

#Source convenience functions
source("https://raw.githubusercontent.com/defleury/Toolbox_16S/master/R/function.alpha_diversity.R")
source("https://raw.githubusercontent.com/defleury/Toolbox_16S/master/R/function.rarefaction.R")
source("https://raw.githubusercontent.com/defleury/Schmidt_et_al_2016_community_similarity/master/functions.community_similarity.R")

#Set basic parameters
PARAM <- list()
PARAM$folder.Rmd <- getwd()
PARAM$folder.base <- gsub("Rmd", "", PARAM$folder.Rmd)
PARAM$folder.data <- paste0(PARAM$folder.base, "data/")
PARAM$folder.results <- paste0(PARAM$folder.base, "results/")

PARAM$use.cores <- 4

#Rarefaction levels (relative sample sizes)
PARAM$rarefaction.levels <- c(seq(0.01, 0.09, by=0.01), seq(0.1, 0.9, by=0.1), 0.99)
#Rarefaction iterations (number of iterations per step)
PARAM$rarefaction.iterations <- 100
#Minimum sample size
PARAM$cutoff.sample_size <- 400
#Minimum OTU size (global, across all samples)
PARAM$cutoff.otu_size <- 4
#Minimum fraction of total samples in which an OTU must have been observed at >= PARAM$cutoff.otu_size
PARAM$cutoff.min_sample_prevalence <- 0.01
#Taxonomy levels
PARAM$tax.levels <- c("phylum", "class", "order", "family", "genus", "species")
```

********
********


## Load and Filter OTU, taxa and Sample Data

Load sample metadata.

```{r}
#Read raw XLSX file, sheet 1 (per-mouse data)
data.sample.mouse <- as.data.frame(read_xlsx(paste0(PARAM$folder.data, "metadata.combined.xlsx"), sheet=1))
data.sample.raw <- as.data.frame(read_xlsx(paste0(PARAM$folder.data, "metadata.combined.xlsx"), sheet=2))

#Re-label rows
rownames(data.sample.raw) <- paste0("sample", as.character(data.sample.raw$sample_id))
```

Load OTU table and OTU data.

```{r}
#Read raw OTU table
ot.raw <- as.matrix(read.table(paste0(PARAM$folder.data, "all_samples.otu_table.gz"), sep="\t", row.names=1, header=T))
colnames(ot.raw) <- gsub("_", ".", colnames(ot.raw))

#Read raw OTU data
data.otu.raw <- read.table(paste0(PARAM$folder.data, "all_samples.otu_data.gz"), sep="\t", row.names = 1, header = T)
```

Load phylogenetic tree

```{r}
file.otu_tree <- paste0(PARAM$folder.data, "all_samples.otu_tree.nwk");
otu.tree.raw <- read_tree(file.otu_tree);
```

Filter data by OTU prevalence and minimum sample size.

```{r}
#Filter samples
#=> keep only samples which satisfy minimnum read count criterion
keep.samples <- intersect(rownames(data.sample.raw), colnames(ot.raw)[colSums(ot.raw) >= PARAM$cutoff.sample_size])

#Filter OTUs
#=> by minimum prevalence
keep.otus <- rownames(ot.raw)[which(rowSums(ot.raw[, keep.samples] >= PARAM$cutoff.otu_size) >= 2)]

#Apply filters
data.otu <- data.otu.raw[keep.otus, PARAM$tax.levels]
ot <- Matrix(ot.raw[keep.otus, keep.samples], sparse=T)

#Re-filter samples to remove those that do not retain reads after OTU filtering
new.keep.samples <- colnames(ot)[colSums(ot) >= (PARAM$cutoff.sample_size * 0.5)]
data.sample <- data.sample.raw[new.keep.samples, ]
ot <- ot[, new.keep.samples]

#Update stats
data.sample$size <- colSums(ot)
data.sample$prevalence <- colSums(ot > 0) / nrow(ot)
data.otu$size <- rowSums(ot)
data.otu$prevalence <- rowSums(ot > 0) / ncol(ot)

#Add NAS_score to data.sample
data.sample$NAS <- NA
for (m in unique(data.sample$mouse_id)) {
  data.sample[data.sample$mouse_id == m, "NAS"] <- data.sample.mouse$`NAS (NAFLD activity score)`[data.sample.mouse$`Mouse ID` == m][1]
}
#Extrapolate remaining NASH scores
data.sample$NAS[is.na(data.sample$NAS)] <- 0

#Filter (prune tree) and root at midpoint
tree.otu <- drop.tip(otu.tree.raw, otu.tree.raw$tip.label[ !otu.tree.raw$tip.label %in% rownames(ot)])
tree.otu <- midpoint.root(tree.otu)
```

These filtering steps removed `r nrow(data.sample.raw) - ncol(ot)` samples due to insufficient read depths (<= `r PARAM$cutoff.sample_size`), and `r nrow(ot.raw) - nrow(ot)` OTUs due to insufficient prevalence (observed with >= `r PARAM$cutoff.otu_size` reads in less than 2 samples). These filtering steps removed `r round(100 * (sum(ot.raw[, keep.samples]) - sum(ot)) / sum(ot.raw[, keep.samples]), digits=2)`% of total reads.

Generate relative abundance tables: normalize each sample by sample size.

```{r}
ot.rel <- t(t(ot) / data.sample$size)
```


Summary statistics on sample sizes:
```{r}
summary(data.sample$size)
quantile(data.sample$size, prob=seq(0,1,0.05))
hist(log10(data.sample$size), 50)
```

In other words, 96% of samples contain >=5,000 sequences: this will provide an appropriate cutoff for rarefied diversity estimates.

Summary statistics on OTU sizes:
```{r}
summary(data.otu$size)

hist(log10(data.otu$size), 50)
```

Store tables for later usage.

```{r}
save(ot, ot.rel, data.otu, tree.otu, file=paste0(PARAM$folder.data, "data.otu.Rdata"))
```

## Rarefaction and Alpha Diversities

Calculate alpha diversities (Hill Diversities) for count tables rarefied to 400 sequences per sample (smaller samples will be dropped). For each sample, diversities at Hill numbers q=0 (richness), q=1 (exp(Shannon)) and q=2 (inverse Simpson) are calculated. Moreover, the Hill-based evenness for q=1 and q=2 are computed.

```{r}
#Calculate raredied Hill diversities
alpha.frame <- Hill_Diversity.rarefied(ot, size=400, iterations=100, q.H=c(0, 1, 2))

#Add Hill evenness
alpha.frame[, "q.1.evenness"] <- alpha.frame$q.1 / alpha.frame$q.0
alpha.frame[, "q.2.evenness"] <- alpha.frame$q.2 / alpha.frame$q.0

#Add to sample data
data.sample <- cbind(data.sample, alpha.frame)

#Store
save(data.sample, file=paste0(PARAM$folder.data, "data.sample.Rdata"))
```

Next, get some summary statistics on the calculated alpha diversities.

```{r}
summary(data.sample$q.0)
summary(data.sample$q.1)
summary(data.sample$q.2)
```

## Beta Diversities

Calculate between-sample compositional similarities (beta diversities) using different indices. The two experiments from the same run are processed separately.

Process `Wyss` samples.

```{r}
sub.ds <- data.sample[data.sample$experiment %in% c("AW12.02", "AW14.01", "AW14.02", "AW19.01", "AW19.02", "AW19.03", "AW19.04", "AW19.05"), ]
sub.ot <- ot[, rownames(sub.ds)]
sub.ot <- sub.ot[rowSums(sub.ot) > 0,]

beta.div <- list();

#Bray-Curtis
beta.div[["Bray_Curtis"]] <- community.similarity.par(sub.ot, distance="bray_curtis", use.cores=PARAM$use.cores)

#Weighted Jaccard
beta.div[["Jaccard_w"]] <- community.similarity.par(sub.ot, distance="jaccard.abd.frac", use.cores=PARAM$use.cores)

#Get pairwise (raw) SparCC correlations and derived "interaction matrix"
ot.sparcc <- sparcc(sub.ot, size.thresh=0, pseudocount=10^-6, nblocks=4, use.cores=PARAM$use.cores)
ot.sparcc.S <- 0.5 * (cor.par(ot.sparcc, nblocks=100, use.cores=PARAM$use.cores) + 1)

#Unweighted TINA
beta.div[["TINA_uw"]] <- community.similarity.corr.par(sub.ot, S=ot.sparcc.S, distance="jaccard.corr.uw.norm", blocksize=100, use.cores=PARAM$use.cores)
beta.div[["TINA_uw"]][beta.div[["TINA_uw"]] < 0] <- 0
rownames(beta.div[["TINA_uw"]]) <- colnames(beta.div[["TINA_uw"]]) <- colnames(sub.ot)

#Weighted TINA
beta.div[["TINA_w"]] <- community.similarity.corr.par(sub.ot, S=ot.sparcc.S, distance="jaccard.corr.w.norm", blocksize=100, use.cores=PARAM$use.cores)
beta.div[["TINA_w"]][beta.div[["TINA_w"]] < 0] <- 0
rownames(beta.div[["TINA_w"]]) <- colnames(beta.div[["TINA_w"]]) <- colnames(sub.ot)

#Store in R format
save(ot.sparcc, file=paste0(PARAM$folder.data, "otu_table.sparcc.wyss.Rdata"))
save(ot.sparcc.S, file=paste0(PARAM$folder.data, "otu_table.sparcc.S.wyss.Rdata"))
save(beta.div, file=paste0(PARAM$folder.data, "data.beta_div.wyss.Rdata"))
```

Process `Raselli` samples.

```{r}
sub.ds <- data.sample[! data.sample$experiment %in% c("AW12.02", "AW14.01", "AW14.02", "AW19.01", "AW19.02", "AW19.03", "AW19.04", "AW19.05"), ]
sub.ot <- ot[, rownames(sub.ds)]
sub.ot <- sub.ot[rowSums(sub.ot) > 0,]

beta.div <- list();

#Bray-Curtis
beta.div[["Bray_Curtis"]] <- community.similarity.par(sub.ot, distance="bray_curtis", use.cores=PARAM$use.cores)

#Weighted Jaccard
beta.div[["Jaccard_w"]] <- community.similarity.par(sub.ot, distance="jaccard.abd.frac", use.cores=PARAM$use.cores)

#Get pairwise (raw) SparCC correlations and derived "interaction matrix"
ot.sparcc <- sparcc(sub.ot, size.thresh=0, pseudocount=10^-6, nblocks=4, use.cores=PARAM$use.cores)
ot.sparcc.S <- 0.5 * (cor.par(ot.sparcc, nblocks=100, use.cores=PARAM$use.cores) + 1)

#Unweighted TINA
beta.div[["TINA_uw"]] <- community.similarity.corr.par(sub.ot, S=ot.sparcc.S, distance="jaccard.corr.uw.norm", blocksize=100, use.cores=PARAM$use.cores)
beta.div[["TINA_uw"]][beta.div[["TINA_uw"]] < 0] <- 0
rownames(beta.div[["TINA_uw"]]) <- colnames(beta.div[["TINA_uw"]]) <- colnames(sub.ot)

#Weighted TINA
beta.div[["TINA_w"]] <- community.similarity.corr.par(sub.ot, S=ot.sparcc.S, distance="jaccard.corr.w.norm", blocksize=100, use.cores=PARAM$use.cores)
beta.div[["TINA_w"]][beta.div[["TINA_w"]] < 0] <- 0
rownames(beta.div[["TINA_w"]]) <- colnames(beta.div[["TINA_w"]]) <- colnames(sub.ot)

#Store in R format
save(ot.sparcc, file=paste0(PARAM$folder.data, "otu_table.sparcc.raselli.Rdata"))
save(ot.sparcc.S, file=paste0(PARAM$folder.data, "otu_table.sparcc.S.raselli.Rdata"))
save(beta.div, file=paste0(PARAM$folder.data, "data.beta_div.raselli.Rdata"))
```




